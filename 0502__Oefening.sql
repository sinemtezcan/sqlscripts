use modernways;
CREATE TABLE Huisdieren(
    Naam VARCHAR(100) CHAR SET utf8mb4 NOT NULL,
    Leeftijd int(300) NOT NULL,
    Soort CHAR(50)  NOT NULL
);