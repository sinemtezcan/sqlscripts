use modernways;
CREATE TABLE Metingen (
    Tijdstip date (yyyy-mm-dd) CHAR SET utf8mb4 NOT NULL,
    Grootte bigint(100) NOT NULL,
    Marge decimal(3,2)
);